# Contribution guide

## Ideas?

Open an issue

 ## Versioning and Deployment

In the project we use versioning based on Semantic Versioning for Python. After a number of changes made to the repository, we make a deployment.

Deployment is made in the following steps:
- Merge a MR with the following changes
    - Bump the version in setup.py
- Create a git tag for the version. This will trigger a CI pipeline which deploys  
```bash
git tag -a <version> -m "<message>"
git push origin <version>
 ```
